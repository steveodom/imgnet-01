import time
import json

import torch
from config import LABELS, LOG_TABLE, EXP_DESCRIPTION, EPOCHS, MODEL_TYPE
import boto3

from s3 import write_via_path
from PersistUtils import sanitize_all

class Log:
    def __init__(self):
      self.conn = boto3.resource('dynamodb', region_name="us-east-1")
      self.table = self.conn.Table(LOG_TABLE)

      self.epochs = {}
      self.current_epoch = {}
      self.accuracy_history = []

    def set_epoch(self, i):
      self.epochs[i] = {"misses": [], "accuracy": 0}
      self.current_epoch = self.epochs[i]
    
    def accuracy(self, accuracy):
      self.current_epoch["accuracy"] = accuracy
    
    def misses(self, i, preds, matches, paths):
      # TODO: can we optimize this with numpy here to avoid loop?
      for ii, match in enumerate(matches):
        if match.int()[0] != 1:
          pred = preds[ii].int()[0]
          prediction_label = LABELS[pred]
          path = paths[ii].split('/')[3]
          label = path.split("_")[0]
          self.current_epoch["misses"].append({
            "pred": prediction_label,
            "actual": label,
            "path": paths[ii],
            "description": path
          })
    
    def write_missed_images(self, experiment_name):
      for miss in self.current_epoch["misses"]:
        prefix = 'imgnet-misses'
        model = experiment_name
        predicted = miss['pred']
        description = miss['description']
        filename = "charts/{}/{}/{}_{}".format(prefix, model, predicted, description)
        write_via_path(miss["path"], filename)

    def save(self, experiment_name):
      self.write_missed_images(experiment_name)
      item = self.current_epoch
      item["history"] = self.accuracy_history
      item["experiement"] = experiment_name
      item["description"] = EXP_DESCRIPTION
      item["epochs"] = EPOCHS
      item["model_type"] = MODEL_TYPE
      item["date_number"] = int(time.time()) 
      item["misses"] = json.dumps(item["misses"])
      item = sanitize_all(item)
      
      response = self.table.put_item(Item=item)
      if response['ResponseMetadata']['HTTPStatusCode'] == 200:
          return True
      else:
          return False