import matplotlib
matplotlib.use('Agg')
# import matplotlib.pyplot as plt

import plotly
import plotly.plotly as py
import plotly.graph_objs as go

import torch
from torch.autograd import Variable
from utils import getFirstRow

from config import PLOT_VALIDATION_DATA

use_gpu = torch.cuda.is_available()


from config import PLOT_EVERY_N_TIMES

class Plot:
    def __init__(self, plotLocation = 'local'):
        self.title = 'Prediction Accuracy for a Epoch'
        self.counter = 0 #used to plot every n times
        
        self.plot_every_n_times = PLOT_EVERY_N_TIMES
        
        plotly.tools.set_credentials_file(username='steveodom', api_key='Q1MvnNgLV2a4CzuLEp3n')
        if plotLocation == 'local':
            self.plot = plotly.offline.plot
        if plotLocation == 'jupyter':
            self.plot = plotly.offline.iplot
        if plotLocation == 'remote':
            self.plot = py.plot
            
    def setup(self):
        self.layout = go.Layout(
            title='Plot Title',
            xaxis=dict(
                title='x Axis',
                titlefont=dict(
                    family='Courier New, monospace',
                    size=18,
                    color='#7f7f7f'
                ),
                autorange=True,
                showgrid=False,
                zeroline=False,
                showline=False,
                autotick=True,
                ticks='',
                showticklabels=False
            ),
            yaxis=dict(
                title='y Axis',
                titlefont=dict(
                    family='Courier New, monospace',
                    size=18,
                    color='#7f7f7f'
                ),
                autorange=True,
                showgrid=False,
                zeroline=False,
                showline=False,
                autotick=True,
                ticks='',
                showticklabels=False
            )
        )

    def scatter(self, items, preds, is_correct, counterOverride = False):
        if (self.counter == self.plot_every_n_times or counterOverride):
            self.trace = go.Scatter(
                x = items,
                y = preds,
                mode = 'markers',
                marker=dict(
                    size="16",
                    color=is_correct,
                    colorscale= [[0, 'red'], [1, 'green']]
                )
            )
    
    def save(self, name, i='0', counterOverride = False):
        if (self.counter == self.plot_every_n_times or counterOverride):
            self.counter = 0
            data = data = [self.trace]
            fig = go.Figure(data=data, layout=self.layout)
            plot_url = self.plot(fig, filename="./plots/{0}{1}".format(name, i), auto_open=False)
        self.counter += 1

    def close(self):
        # plt.close()
        return False
    

    @staticmethod
    def epoch_accuracy_calc(matches, preds, epoch_predictions, epoch_prediction_accuracy):
        longtensor = torch.cuda.LongTensor if use_gpu else torch.LongTensor
        bytetensor = torch.cuda.ByteTensor if use_gpu else torch.ByteTensor
        
        if epoch_predictions is None:
          epoch_predictions = longtensor(1, 1).zero_()
          epoch_prediction_accuracy = bytetensor(1, 1).zero_()
        
        rowAccuracy = getFirstRow(matches, use_gpu) # the tensor is 6x6 say, but each row is the same. So we only get the first row.
        rowPredictions = torch.t(preds)
        epoch_prediction_accuracy = torch.cat((epoch_prediction_accuracy, rowAccuracy), 1)
        epoch_predictions = torch.cat((epoch_predictions, rowPredictions), 1)
        return epoch_predictions, epoch_prediction_accuracy
  