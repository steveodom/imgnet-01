import torch
from torch.autograd import Variable
from Plot import Plot

from utils import print_accuracy
from config import PLOT_TRAINING_DATA, DEBUG_TRAINING, BATCH_SIZE

use_gpu = torch.cuda.is_available()

class Trainer:
    def __init__(self, net, optimizer, criterion, Logger):
      self.net = net
      self.optimizer = optimizer
      self.criterion = criterion
      net.train()
      self.Logger = Logger
      
    def epoch(self, loader, epoch_num):
      running_loss = 0
      running_accuracy = 0
      running_total = 0
      epoch_predictions, epoch_prediction_accuracy = None, None
      
      for i, (X,y,paths) in enumerate(loader):
        if use_gpu:
              X, y = X.cuda(), y.cuda()
        inputs = Variable(X, requires_grad=True) 
        labels = self.net.label_variable(y)
        
        self.optimizer.zero_grad()
        output = self.net(inputs)
        loss = self.criterion(output, labels)  
        loss.backward()
        self.optimizer.step()

        running_loss += loss.data[0]
        running_total += BATCH_SIZE
        
        preds, matches = self.net.prediction_and_matches(output, labels)
        running_accuracy += torch.sum(matches)

        if DEBUG_TRAINING:
          print_accuracy('train', torch.sum(matches), running_accuracy, running_total, running_loss)

        if PLOT_TRAINING_DATA:
          epoch_predictions, epoch_prediction_accuracy = Plot.epoch_accuracy_calc(matches, preds, epoch_predictions, epoch_prediction_accuracy)
        
      return running_loss/len(loader), running_accuracy/running_total, epoch_predictions, epoch_prediction_accuracy