import numbers
from collections import Mapping, Iterable, Set
from decimal import Decimal 
from math import isnan
import time
import datetime

def sanitize_all(columns):
      for key in columns:
        column = columns[key]
        sanitized = _sanitize(column)
        #print(type(sanitized).__name__, key, sanitized)
        columns[key] = sanitized
      
      return columns

def _sanitize(item):
      # don't catch str/bytes with Iterable check below;
      # don't catch bool with numbers.Number
      if isinstance(item, (str, bool)):
          return item
      
      # ignore inexact, rounding errors
      if isinstance(item, (numbers.Number, float)):
          if isnan(item):
            item = 0
          return Decimal(str(item))
      
      # mappings are also Iterable
      elif isinstance(item, Mapping):
          return {
              key: _sanitize(item)
              for key, value in item.values()
          }

      # boto3's dynamodb.TypeSerializer checks isinstance(o, Set)
      # so we can't handle this as a list
      elif isinstance(item, Set):
          return set(map(_sanitize, item))
      
      # may not be a literal instance of list
      elif isinstance(item, Iterable):
          return list(map(_sanitize, item))
      
      # datetime, custom object, None
      return item