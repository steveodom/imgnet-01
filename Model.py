#
# Load a pretrained model and reset final fully connected layer.
#
import torch
import torch.nn as nn
import torch.optim as optim
import torchvision.models as models

from models.cnn import CNN
from models.mnist import MNIST
from models.lstm import Sequence
from models.dcgan import netD

use_gpu = torch.cuda.is_available()

class Model:
    def __init__(self, klass):    
        self.model = None
        self.criterion = None
    
        options = {
            'cnn': self.cnn,
            'mnist': self.mnist,
            'sequence': self.sequence,
            'vgg': self.vgg,
            'dcgan': self.dcgan
        }

        # call the model we'd like to use
        options[klass]()
        
        if use_gpu:
          self.model = self.model.cuda()

    def cnn(self):
        self.model = CNN()
    
    def mnist(self):
        self.model = MNIST()
    
    def sequence(self):
        self.model = Sequence()
    
    def vgg(self):
        self.model = models.vgg16()
    
    def dcgan(self):
        self.model = netD()