import torch

import torch.optim as optim
from torch.optim import lr_scheduler
from torch.autograd import Variable

from config import EPOCHS, LEARNING_RATE, MODEL_TYPE, PRINT_INTERVAL, PLOT_TRAINING_DATA, EXP_NAME, LOAD_DICT, LABELS

import numpy as np
import time

from Loaders import Loaders
from Model import Model
from Trainer import Trainer
from Validater import Validater

from Plot import Plot
from Log import Log

print(torch.__version__, 'pytorch')

torch.backends.cudnn.benchmark = True
torch.backends.cudnn.fastest = True

train_loader, val_loader = Loaders().fetch(LABELS)

net = Model(MODEL_TYPE)

if LOAD_DICT:
    net.model.load_state_dict(torch.load(LOAD_DICT, map_location=lambda storage, loc: storage))

optimizer = optim.Adam(net.model.parameters(), lr=LEARNING_RATE)
current_lr = optimizer.param_groups[0]['lr']

criterion =  net.model.criterion()
scheduler = lr_scheduler.ReduceLROnPlateau(optimizer, 'min', verbose=True)

plot = Plot()
plot.setup()
val_loss = None
LoggerTrainer = Log()
LoggerValidator = Log()

trainer = Trainer(net.model, optimizer, criterion, LoggerTrainer)
validater = Validater(net.model, optimizer, criterion, LoggerValidator)


for e in range(EPOCHS):
    LoggerValidator.set_epoch(e)
    LoggerTrainer.set_epoch(e)

    start = time.time()
    # stop training if our learning rate reaches a plateau.
    if float(current_lr) < 1e-5:
        print('less than')
        break

    train_loss, train_acc, train_predictions, train_predictionAccuracy = trainer.epoch(train_loader, e)
    val_loss, val_acc, val_predictions, val_predictionAccuracy = validater.epoch(val_loader, e)
    scheduler.step(val_loss)
    
    LoggerValidator.accuracy_history.append(val_acc)
    LoggerValidator.accuracy(val_acc)

    end = time.time()
    
    if e % PRINT_INTERVAL == 0: 
        stats = """Epoch: {}\t train loss: {:.3f}, train acc: {:.3f} time: {:.1f}s""".format( e, train_loss, train_acc, end-start)
        print(stats)
    
    # plotting
    if PLOT_TRAINING_DATA:
        pred_list = train_predictions.numpy()[0]
        accuracy_list = train_predictionAccuracy.numpy()[0]
        plot.scatter(np.arange(len(pred_list)), pred_list, accuracy_list)
        plot.save('train', e)

LoggerValidator.save(EXP_NAME)
print('training stop')
torch.save(net.model.state_dict(), 'saves/%s.pth' % (EXP_NAME))