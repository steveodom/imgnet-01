import torch 
import torch.nn as nn
from torch.autograd import Variable

use_gpu = torch.cuda.is_available()

ngpu = int(1)
nz = int(100)
ngf = int(64)
ndf = int(8)
nc = 3

# Copied over the discrimator from the gan/dcgan file
# matching up so we can use it's pth weights.

# A DCGAN predicts a binary output. Is it or isn't it.abs
# So I can't figure out how to convert it to a classifier which will predict it as 1 of n classes
class netD(nn.Module):
    def __init__(self):
        super(netD, self).__init__()
        self.ngpu = ngpu
        self.main = nn.Sequential(
            # input is (nc) x 64 x 64
            nn.Conv2d(nc, ndf, 4, 2, 1, bias=False),
            nn.LeakyReLU(0.2, inplace=True),
            # state size. (ndf) x 32 x 32
            nn.Conv2d(ndf, ndf * 2, 4, 2, 1, bias=False),
            nn.BatchNorm2d(ndf * 2),
            nn.LeakyReLU(0.2, inplace=True),
            # state size. (ndf*2) x 16 x 16
            nn.Conv2d(ndf * 2, ndf * 4, 4, 2, 1, bias=False),
            nn.BatchNorm2d(ndf * 4),
            nn.LeakyReLU(0.2, inplace=True),
            # state size. (ndf*4) x 8 x 8
            nn.Conv2d(ndf * 4, ndf * 8, 4, 2, 1, bias=False),
            nn.BatchNorm2d(ndf * 8),
            nn.LeakyReLU(0.2, inplace=True),
            # state size. (ndf*8) x 4 x 4
            nn.Conv2d(ndf * 8, 1, 4, 1, 0, bias=False),
            nn.Sigmoid()
        )

    def forward(self, input):
        if isinstance(input.data, torch.cuda.FloatTensor) and self.ngpu > 1:
            out = nn.parallel.data_parallel(self.main, input, range(self.ngpu))
        else:
            out = self.main(input)

        return out.view(-1, 1).squeeze(1)
    
    def criterion(self):
        return nn.BCELoss()
    
    def label_variable(self, y):
        floattensor = torch.cuda.FloatTensor if use_gpu else torch.FloatTensor
        return Variable(y).type(floattensor)
    
    def prediction_and_matches(self, output, labels):
        preds = output.data.round()
        matches = preds == labels.data
        return preds, matches