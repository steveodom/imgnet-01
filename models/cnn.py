import torch 
import torch.nn as nn
from torch.autograd import Variable

number_of_classes = 3
class CNN(nn.Module):
    def __init__(self):
        super(CNN, self).__init__()
        output1 = 16
        output2 = 32
        kernel_size=5
        padding=2
        
        self.layer1 = nn.Sequential(
            # first arg in Conv2d is how many channels.
            # color images are 3 channels so it expects 3
            nn.Conv2d(3, output1, kernel_size=kernel_size, padding=padding),
            nn.BatchNorm2d(output1),
            nn.ReLU(),
            nn.MaxPool2d(2))
        self.layer2 = nn.Sequential(
            nn.Conv2d(output1, output2, kernel_size=kernel_size, padding=padding),
            nn.Dropout2d(p=0.7),
            nn.BatchNorm2d(output2),
            nn.ReLU(),
            nn.MaxPool2d(2))
        
        #args for Linear is size of input sample, and size of output
        # the 8 * 8 below is derived from 32 original size / 2 for the first MaxPool2D / 2 for the second MaxPool2d
        self.fc = nn.Linear(output2*16*16, number_of_classes)
        
    def forward(self, input):
        out = self.layer1(input)
        # the size of the first layer is the batch_size, 
        # the second dimenision is output1
        # the third and fourth dimensions are the height and width of the image divided by the maxPool kernel size.
        # so if the image's height is 32 and the MaxPool2D(2). It decreases the size to 16, 16.
        # there are 3 output_1's because there are 3 channels
        #print('first out', out.size())
        out = self.layer2(out)
        #print('second out', out.size())
        # this reshapes it where it has 4 rows and (-1) as many columns as it needs to allocate the data
        
        out = out.view(out.size(0), -1)
        #print('third out', out.size())
        
        out = self.fc(out)
        #print('out', out)
        return out

    def criterion(self):
        return nn.CrossEntropyLoss()

    def label_variable(self, y):
        return Variable(y)

    def prediction_and_matches(self, output, labels):
        preds = output.data.max(1, keepdim=True)[1] # one here is the indexes of the max numbers
        matches = preds == labels.data.view_as(preds)
        return preds, matches