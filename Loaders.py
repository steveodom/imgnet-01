import torch
import torchvision.transforms as transforms
from torch.utils.data.sampler import SubsetRandomSampler
from torch.utils.data import DataLoader

from DataBuilder import DataBuilder
from utils import train_valid_split
from config import BATCH_SIZE, DROP_LAST

use_gpu = torch.cuda.is_available()

class Loaders:
    def __init__(self):
      pass

    def transform(self):  
      return transforms.Compose(
        [
          transforms.Resize((64,64)),
          transforms.ToTensor()
          #transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))])
          # transforms.Normalize(mean=[0.485, 0.456, 0.406],std=[0.229, 0.224, 0.225])])
        ])
    
    def fetch(self, labels=[]):
      dataset = DataBuilder('./data/', self.transform(), labels)
      train_idx, valid_idx = train_valid_split(dataset, 0.25, True)

      train_sampler = SubsetRandomSampler(train_idx)
      valid_sampler = SubsetRandomSampler(valid_idx)

      train_loader = DataLoader(
          dataset, 
          sampler=train_sampler,
          batch_size=BATCH_SIZE, 
          num_workers=4, 
          drop_last=DROP_LAST,
          pin_memory=use_gpu
      )

      val_loader = DataLoader(
          dataset, 
          sampler=valid_sampler, 
          batch_size=BATCH_SIZE,
          num_workers=4,
          drop_last=DROP_LAST,
          pin_memory=use_gpu
      )

      return train_loader, val_loader