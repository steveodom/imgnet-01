# to run:
python3 main.py

# Instance Type for GPU:
I used: p2.xlarge. super fast.

# login
ssh -v -i ../pytorch.pem ubuntu@54.204.81.5

# copy files up
scp -i ../../pytorch.pem -r images.zip ubuntu@34.228.20.229:~/imgnet-01/data/

# zip files to upload
zip -r images.zip updown/

#unzip a data file on the server:
unzip data/images.zip -d data/

Note, when zipped it added the directory "updown" to the names. When unzipping, it will create this directory. So don't say -d data/updown.

# Updating Pytorch and Torchvision on EC2
sudo pip3 install http://download.pytorch.org/whl/cu80/torch-0.3.0.post4-cp35-cp35m-linux_x86_64.whl
sudo pip3 uninstall torchvision
sudo pip3 install torchvision
