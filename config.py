LABELS = ['down','flat','up']
BATCH_SIZE = 4
EPOCHS = 100
EXP_NAME = "simple-recent-price-up-or-down" #experiment name. Used to save the dict.
EXP_DESCRIPTION = "Simple convolutional net that takes stock chart and determines whether the last 7 periods were up, down, or flat on a 45 period chart. The hypothesis is that most of the errors would be where the model can't quite tell the difference between flat and up, or flat and down, if the change is near the threshold between the two. It's easy for us to be unsure here as well. This experiment is a prelude to trickier requirements."
LOAD_DICT = "" #"saves/price-simple.pth"
LEARNING_RATE = 1e-4
MODEL_TYPE = 'cnn'
PRINT_INTERVAL = 50
DROP_LAST = False

LOG_TABLE = "ImgNetResults"

PLOT_COUNT = 10
PLOT_EVERY_N_TIMES = EPOCHS / PLOT_COUNT # 10 plotCount should generate 10 plots

PLOT_TRAINING_DATA = False
DEBUG_TRAINING = True
PLOT_VALIDATION_DATA = False
DEBUG_VALIDATION = True