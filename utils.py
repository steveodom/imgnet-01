import torch
import random
from math import floor

from config import BATCH_SIZE

def getFirstRow(t, use_gpu):
  longtensor = torch.cuda.LongTensor if use_gpu else torch.LongTensor
  indices = longtensor([0]) #the 1 can be any number.
  dim = 0 # 1 would give you the column
  return torch.index_select(t, 0, indices)


def train_valid_split(dataset, test_size = 0.25, shuffle = False, random_seed = 0):
    """ Return a list of splitted indices from a DataSet.
    Indices can be used with DataLoader to build a train and validation set.
    
    Arguments:
        A Dataset
        A test_size, as a float between 0 and 1 (percentage split) or as an int (fixed number split)
        Shuffling True or False
        Random seed
    """
    length = len(dataset)
    indices = list(range(1,length))
    
    if shuffle == True:
        random.seed(random_seed)
        random.shuffle(indices)
    
    if type(test_size) is float:
        split = floor(test_size * length)
    elif type(test_size) is int:
        split = test_size
    else:
        raise ValueError('%s should be an int or a float' % str)
    return indices[split:], indices[:split]

def print_accuracy(mode, match_sum, running_accuracy, running_total, running_loss):
        print(mode, "PCT: ", match_sum/BATCH_SIZE, "Running PCT: %d%%" % (round(running_accuracy/running_total*100,2)), "Loss", round(running_loss, 2))    