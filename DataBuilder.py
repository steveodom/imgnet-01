
# Reference class as example to overwrite
# https://github.com/pytorch/vision/blob/master/torchvision/datasets/folder.py#L66

# a useful example
# https://www.kaggle.com/demobin/fashion-mnist-pytorch
import torch
import torch.utils.data as data

import pandas as pd # data processing, CSV file I/O (e.g. pd.read_csv)
import numpy as np

import os
import glob

from torchvision import get_image_backend
# import accimage
from PIL import Image

# Label Examples
# torch.IntTensor(['down','down-flat','flat','up-flat','up'])
# torch.IntTensor([0,1,2,3,4])
# ['down','down-flat','flat','up-flat','up']
# ['down','flat','up']
# ['hs', 'ihs']


class DataBuilder(data.Dataset):
    def __init__(self, path, transform=None, labels=[]):
        # labels are set in config.
        self.labels = labels #self.get_labels()
        self.features = self.get_images(path)
        self.num_of_features = len(self.features)     
        self.num_of_labels = len(self.labels)
    
        self.transform = transform

    def get_images(self, path):
        images = []
        for dirname, dirnames, filenames in os.walk(path):
            # print path to all filenames.
            for filename in filenames:
                if not filename.startswith('.'):
                    label = filename.split("_")[0]
                    # TODO: remove the 'flat' check. For testing purposes for DCGAN.
                    if label != 'nan':
                        label_index = self.labels.index(label)
                        path = os.path.join(dirname, filename)
                        images.append({'path': path, 'label': label_index})
        return images

    def __len__(self):
        return len(self.features)

    def __getitem__(self, i):
        feature = self.features[i]
        path, label = feature['path'], feature['label']
        image = self.default_loader(path)
        if self.transform is not None:
            image = self.transform(image)
        return image, label, path
    
    # Image loading stuff copied from https://github.com/pytorch/vision/blob/master/torchvision/datasets/folder.py
    def pil_loader(self, path):
        # open path as file to avoid ResourceWarning (https://github.com/python-pillow/Pillow/issues/835)
        with open(path, 'rb') as f:
            with Image.open(f) as img:
                return img.convert('RGB')


    # def accimage_loader(self, path):
    #     try:
    #         return accimage.Image(path)
    #     except IOError:
    #         # Potentially a decoding problem, fall back to PIL.Image
    #         return pil_loader(path)

    def default_loader(self, path):
        # if get_image_backend() == 'accimage':
        #     return self.accimage_loader(path)
        # else:
        return self.pil_loader(path)
